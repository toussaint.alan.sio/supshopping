package dao;

import entity.Product;

import javax.ejb.Local;
import java.util.List;

@Local
public interface ProductDao {

    long retrieveProductNumber();

    Product addProduct(Product product);

    List<Product> findAll();

    Product findProduct(long id);

}
