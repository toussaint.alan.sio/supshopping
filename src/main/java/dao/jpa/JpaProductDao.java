package dao.jpa;

import dao.ProductDao;
import entity.Product;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class JpaProductDao implements ProductDao {
    @PersistenceContext(name = "supshopping")
    private EntityManager em;

    public long retrieveProductNumber() {
        long count = 0;

        count = (long) em.createNamedQuery("product.nber").getSingleResult();
        System.out.println("COUNT : " + count);
        return count;
    }

    public Product addProduct(Product product) {

        try{
            em.persist(product);
            return product;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Product> findAll() {
        return this.em.createNamedQuery("product.all")
                .getResultList();
    }


    @Override
    public Product findProduct(long id) {

        Product prod = null;

        prod = (Product) em.createNamedQuery("product.oneproduct")
                .setParameter("id", id)
                .getSingleResult();
        System.out.println("PROD : " + prod.getProductName());
        return prod;
    }
}
