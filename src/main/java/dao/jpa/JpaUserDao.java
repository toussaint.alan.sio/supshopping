package dao.jpa;




import dao.UserDao;
import entity.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Stateless
public class JpaUserDao implements UserDao {

    @PersistenceContext(name = "supshopping")
    private EntityManager em;


    @Override
    public Optional<User> authenticate(String userName, String password) {
        User unUser;
        try {
            // Get the authenticated user using the named query.
            unUser = (User) em.createNamedQuery("user.authenticate")
                    .setParameter("username", userName)
                    .setParameter("password", password)
                    .getSingleResult();
        } catch (NoResultException nre) {
            // If there is no result then return an empty optional.
            nre.printStackTrace();
            return Optional.empty();
        }

        return Optional.of(unUser);
    }

    public long count(){
        long count = 0;

        count = (long) em.createNamedQuery("user.nber").getSingleResult();
        System.out.println("COUNT : " + count);
        return count;
    }

    public User checkUser(String username, String password){
        try { User user = (User) em .createQuery( "SELECT u from User u where u.username = :name and u.password = :password")
                .setParameter("name", username)
                .setParameter("password", password)
                .getSingleResult();
            return user;
        } catch (NoResultException e) {
            e.printStackTrace();
            return null;
        }

    }

    public User addUser(User user) {
        try{
            em.persist(user);
            return user;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public User modifUser(User user) {
        try{
            em.merge(user);
            return user;
        }catch (Exception e){
            e.printStackTrace();
            return user;
        }

    }
}
