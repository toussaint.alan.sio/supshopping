package dao;

import entity.User;

import javax.ejb.Local;
import java.util.Optional;


@Local
public interface UserDao {

    User checkUser(String username, String password);

    User addUser(User user);

    User modifUser(User user);

    Optional<User> authenticate(String userName, String password);

    long count();

}
