package Controller;

import dao.UserDao;
import entity.User;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

@SessionScoped
@ManagedBean(name = "userBean")
public class UserController implements Serializable {

    private String username;
    private String password;
    private User connectedUser;

    private ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
    private Map<String, Object> sessionMap = externalContext.getSessionMap();

    private User user = new User();

    @EJB
    private UserDao userDao;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {this.password = password;}

    public User getConnectedUser() {
        return connectedUser;
    }

    public void setConnectedUser(User connectedUser) {
        this.connectedUser = connectedUser;
    }

    public String login() {
        Optional<User> user = this.userDao.authenticate(username, password);
        if (!user.isPresent()) {
            return "/jsf/Failed.xhtml";
        } else {
            // Set the connected user
            this.connectedUser = user.get();
            sessionMap.put("user", connectedUser);
            return "/jsf/Success.xhtml";
        }

    }

    public void logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().invalidateSession();
        try {
            //context.getExternalContext().redirect("/shopping/jsf/index.xhtml");
            context.getExternalContext().redirect("/jsf/index.xhtml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String addNewUser() {
        userDao.addUser(user);
        this.connectedUser = user;
        sessionMap.put("user", connectedUser);
        return "/jsf/private/profil.xhtml";
    }

    public void modifUser() {
        connectedUser = (User) sessionMap.get("user");
        user.setId(connectedUser.getId());
        userDao.modifUser(user);
    }

    public long count(){
        return userDao.count();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
