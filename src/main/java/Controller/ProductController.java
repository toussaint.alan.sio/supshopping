package Controller;

import dao.ProductDao;
import entity.Product;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.ListDataModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SessionScoped
@ManagedBean(name = "productBean")
public class ProductController implements Serializable {


    @EJB
    private ProductDao productDao;

    private List listProduct;

    private Product product = new Product ();

    public ProductController() {
    }

    public void addNewProduct() {productDao.addProduct(product);}

    public long retrieveProductNumber() {
        return productDao.retrieveProductNumber();
    }

    public String findProduct(long id) {

        product = productDao.findProduct(id);
        System.out.println("dromo : " + product.getProductName());
        return "/jsf/private/productDetail.xhtml";
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List getListProduct() {
        listProduct = new ArrayList<Product>();

        for(Product p : productDao.findAll()){
            System.out.println("VOLVO : " + p.getProductName());
            listProduct.add(p);
        }

        return listProduct;
    }

    public void setListProduct(List<Product> listProduct) {
        this.listProduct = listProduct;
    }

}
